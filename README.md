# Making a Bootable OS USB Drive With WoeUSB

`Learn how to make a bootable Linux OS USB with WoeUSB!`

### Choosing a Distro

1. **Visit [DistroWatch](https://distrowatch.com)**
2. **What are your interests/criteria?**
3. **Which one did Peyton choose?** Pop!_OS
4. **Why?** Pop!_OS is specifically marketed as a gaming OS 
5. **Download the ISO:** https://pop.system76.com/

### Download WoeUSB
0. **To install WoeUSB on Ubuntu 22.04, run the following commands.**
1. `sudo add-apt-repository ppa:tomtomtom/woeusb -y`
2. `sudo apt update && sudo apt upgrade`
3. `sudo apt install woeusb woeusb-frontend-wxgtk -y`
4. `woeusb --version`
5. **Open WoeUSB with** `woeusbgui'
6. There are other way to use WoeUSB but using the GUI is the easiest for me.

### Write the ISO to the USB Stick
0. Make sure there are no partitions on your USB drive. 
1. Select your ISO file in the GUI
2. Select FAT filesystem (usually)
3. Hit "Install"

### Boot With USB Stick

1. Restart and hit your Boot Menu button. 
2. Choose the USB stick you installed the OS on
3. Follow instructions within the OS to install it on the system!

### Done; Q&A
